#!/usr/bin/env sh

psql -d postgres -c "CREATE EXTENSION plpythonu;"
psql -d postgres -c "CREATE EXTENSION postgis;"

psql -d postgres -c "CREATE EXTENSION postgis_topology;"
psql -d postgres -c "CREATE EXTENSION postgis_sfcgal;"

MAKEFLAGS='-j1' pgxn install madlib \
    && pgxn load madlib

/usr/local/madlib/bin/madpack -s madlib -p postgres -c postgres@localhost:5432/postgres install
/usr/local/madlib/bin/madpack -s madlib -p postgres -c postgres@localhost:5432/postgres install-check
