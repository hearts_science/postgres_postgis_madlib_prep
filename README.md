# Postgres Extended

This repo is meant to provide the recipe for building an image with
[MADlib](https://madlib.apache.org) and [PostGIS](https://postgis.net/) from a
container.

These extensions require certain steps to happen post database initialization.
It is simpler to prepare an image with all the requirements and execute the
steps in the container. Once everything is set up an image is created from the
container via [`docker
commit`](https://docs.docker.com/engine/reference/commandline/commit/)


## Base Image

The Dockerfile uses the 'official' Postgres image.

The image created has all the required libraries to successfully install MADlib
and PostGIS.


## Installing the Extensions

Once the Base Image is built, run the container:

```bash
docker run --name pg_building postgres
```

and follow by:

```bash
docker exec -it pg_building bash
```

Once in the container the required steps can be executed to build the MADlib
and create the necessary extensions.

Execute all the steps in the `install.sh` script.


This will install MADlib and **deploy** it for the postgres database only.

It needs to be deployed separately for each database created:

```bash
/usr/local/madlib/bin/madpack -s madlib -p postgres -c [user[/password]@][host][:port][/database] install
```
